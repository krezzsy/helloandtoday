import com.kncept.junit5.reporter.gradle.TestHTMLReporterPlugin
import com.kncept.junit5.reporter.gradle.TestHTMLReporterSettings
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.junit.platform.gradle.plugin.JUnitPlatformExtension
import org.junit.platform.gradle.plugin.JUnitPlatformPlugin

group = "by.bsuir.labs.tests"
version = "1.0-SNAPSHOT"

buildscript {
    repositories {
        jcenter()
    }

    dependencies {
        classpath("org.junit.platform:junit-platform-gradle-plugin:1.0.3")
        classpath("com.kncept.junit5.reporter:junit-reporter:1.0.2")
    }
}

plugins {
    kotlin("jvm") version ("1.2.20")
}

apply<JUnitPlatformPlugin>()
apply<TestHTMLReporterPlugin>()

repositories {
    jcenter()

    maven("http://dl.bintray.com/jetbrains/spek")
}

val kotlinVersion by project
val jupiterVersion by project
val vintageVersion by project
val kluentVersion by project
val mockitoVersion by project
val spekVersion by project

dependencies {
    compile(kotlin("stdlib-jre8", "${kotlinVersion}"))

    testCompile("org.junit.jupiter:junit-jupiter-api:${jupiterVersion}")
    testCompile("org.junit.jupiter:junit-jupiter-params:${jupiterVersion}")
    testCompile("org.amshove.kluent:kluent:${kluentVersion}")
    testCompile("org.mockito:mockito-core:${mockitoVersion}")
    testCompile("org.jetbrains.spek:spek-api:${spekVersion}")

    testRuntime("org.junit.jupiter:junit-jupiter-engine:${jupiterVersion}")
    testRuntime("org.junit.vintage:junit-vintage-engine:${vintageVersion}")
    testRuntime("org.jetbrains.spek:spek-junit-platform-engine:${spekVersion}")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType(KotlinCompile::class.java).all {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

tasks["junitPlatformTest"].finalizedBy("junitHtmlReport")

configure<TestHTMLReporterSettings> {
    isAggregated = false
}

task<Wrapper>("wrapper") {
    gradleVersion = "4.4.1"
    distributionType = Wrapper.DistributionType.ALL
}


