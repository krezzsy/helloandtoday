package by.bsuir.labs.tests

import org.junit.jupiter.api.Assumptions
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.RepetitionInfo

class RepeatedTest {
    @RepeatedTest(9)
    fun bottlesOfBeer(info: RepetitionInfo) {
        println("${info.totalRepetitions - info.currentRepetition + 1} bottles of beer on the wall, ${info.totalRepetitions - info.currentRepetition + 1} bottles of beer.")
        println("Take one down, pass it around, ${info.totalRepetitions - info.currentRepetition} bottles of beer on the wall...")
        Assumptions.assumingThat(info.currentRepetition == 99) {
            println("No more bottles of beer on the wall, no more bottles of beer.")
            println("Go to the store and buy some more, 99 bottles of beer on the wall...")
        }
    }
}
