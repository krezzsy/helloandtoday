package by.bsuir.labs.tests

import org.apiguardian.api.API
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.*
import org.junit.jupiter.params.support.AnnotationConsumer
import java.lang.annotation.*
import java.util.stream.Stream

class ParametrizedTest {

    @ParameterizedTest
    @ValueSource(strings = ["Hello and today!", "India, India, India..."])
    fun hello(value: String) {
        println(value)
    }

    @ParameterizedTest
    @CsvFileSource(resources = arrayOf("/Characters.csv"))
    fun characters(name: String, race: String) {
        println("${name}: ${race}")
    }

    @ParameterizedTest
    @EnumSource(value = Race::class)
    fun races(race: Race) {
        println(race)
    }

    @ParameterizedTest
    @AnswerSource
    fun answer(answer: Int) {
        Assertions.assertEquals(42, answer)
    }
}

enum class Race {
    HUMAN, BETELGEUSIAN
}

@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@API(status = API.Status.EXPERIMENTAL, since = "1.0")
@ArgumentsSource(AnswerSourceProvider::class)
annotation class AnswerSource()

class AnswerSourceProvider : ArgumentsProvider, AnnotationConsumer<AnswerSource> {
    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> {
        return listOf(42).stream().map { Arguments.of(it) }
    }

    override fun accept(t: AnswerSource?) {
    }
}
